package main

import (
	"bufio"
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

func ConnectToMySQL(db *sql.DB, detailedSql JSONFile) *sql.DB {
	driverName := ""
	uri := ""
	key, _ := hex.DecodeString(EncKey)
	nonce, _ := hex.DecodeString(EncNonce)
	sqlUsername := string(decrypt(key, nonce, []byte(detailedSql.SQLDetails.Username)))
	sqlPassword := string(decrypt(key, nonce, []byte(detailedSql.SQLDetails.Password)))
	log.Println("")
	/*if strings.ToLower(detailedSql.SQLDetails.Type) == "oracle" {
		uri = sqlUsername + "/" + sqlPassword + "@" + detailedSql.SQLDetails.SQLURI + ":" + strconv.Itoa(detailedSql.SQLDetails.Port) + "/" + detailedSql.SQLDetails.InstanceName
		driverName = "goracle"
	} else*/if strings.ToLower(detailedSql.SQLDetails.Type) == "mysql" {
		uri = sqlUsername + ":" + sqlPassword + "@tcp(" + detailedSql.SQLDetails.SQLURI + ":" + strconv.Itoa(detailedSql.SQLDetails.Port) + ")/" + detailedSql.SQLDetails.InstanceName
		log.Println("aaaaaaaaaaaaaaaaaaaaaaaaaaaa", uri)
		log.Println("aaaaaaaaaa", detailedSql.SQLDetails.SQLURI)
		log.Println("aaaaaaaaaaaaaaaaaaaaaaaaaaaa", strconv.Itoa(detailedSql.SQLDetails.Port))
		driverName = "mysql"
	} else if strings.ToLower(detailedSql.SQLDetails.Type) == "mssql" {
		uri = fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;instance=%s;database=%s", detailedSql.SQLDetails.SQLURI, sqlUsername, sqlPassword, detailedSql.SQLDetails.Port, detailedSql.SQLDetails.InstanceName, detailedSql.SQLDetails.Database)
		driverName = "sqlserver"
	}
	db, err := sql.Open(driverName, uri)
	if err != nil {
		log.Fatal(err)
	}
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connected to MySQL")
	return db
}

func GetColumnDetails(rows *sql.Rows, db *sql.DB, tableName, dbType string) *sql.Rows {
	var err error
	if dbType == "mysql" {
		rows, err = db.Query("SELECT COLUMN_NAME,DATA_TYPE,COLUMN_KEY FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=?;", tableName)
		if err != nil {
			log.Fatal(err)
		}
	} else if dbType == "mssql" {
		query := fmt.Sprintf("SELECT COLUMN_NAME,DATA_TYPE,IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='%s';", tableName)
		rows, err = db.Query(query)
		if err != nil {
			log.Fatal(err)
		}
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return rows
}

func ReadPropertiesFile(filename string) (AppConfigProperties, error) {
	config := AppConfigProperties{}

	if len(filename) == 0 {
		return config, nil
	}
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if equal := strings.Index(line, "="); equal >= 0 {
			if key := strings.TrimSpace(line[:equal]); len(key) > 0 {
				value := ""
				if len(line) > equal {
					value = strings.TrimSpace(line[equal+1:])
				}
				config[key] = value
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
		return nil, err
	}

	return config, nil
}

func ReadJSONFile(filename string) JSONFile {
	var jsonData JSONFile
	byteValue, _ := ioutil.ReadFile(filename)

	json.Unmarshal(byteValue, &jsonData)
	return jsonData
}

func AppendID(insideID map[string]interface{}, IDproperties map[string]string, definitions map[string]interface{}, tableName string) map[string]interface{} {
	insideID["prefix"] = strings.ToUpper(tableName[0:3])
	insideID["counter"] = 1001
	IDproperties["name"] = "ID"
	IDproperties["dataPath"] = "_id"
	insideID["properties"] = IDproperties
	definitions["_id"] = insideID
	return definitions
}

func AppendRest(rows *sql.Rows, props AppConfigProperties, definitions map[string]interface{}, tableName, dbType string) map[string]interface{} {
	var colName string
	var dataType string
	var keyVal string
	for rows.Next() {
		err := rows.Scan(&colName, &dataType, &keyVal)
		if err != nil {
			log.Fatal(err)
		}

		insideRest := make(map[string]interface{})
		properties := make(map[string]interface{})
		colName = strings.Replace(colName, "_", " ", -1)
		colName = strings.Replace(colName, "-", " ", -1)
		camelColName := strings.ToLower(string(colName[0])) + colName[1:]
		camelColName = strings.Replace(camelColName, " ", "", -1)
		titleColName := strings.Title(strings.ToLower(colName))
		properties["name"] = titleColName
		properties["dataPath"] = strings.ToLower(camelColName)
		if dbType == "mysql" {
			if keyVal == "PRI" {
				properties["required"] = true
			}
		} else if dbType == "mssql" {
			if keyVal == "NO" {
				properties["required"] = true
			}
		}
		insideRest["type"] = props[dataType]
		insideRest["properties"] = properties
		definitions[titleColName] = insideRest
	}

	return definitions
}

func CreateMaster(definitions map[string]interface{}, tableName string, appName string) map[string]interface{} {
	masterData := make(map[string]interface{})
	masterData["name"] = tableName
	camelTableName := strings.ToLower(string(tableName[0])) + tableName[1:]
	camelTableName = strings.Replace(camelTableName, "_", "", -1)
	camelTableName = strings.Replace(camelTableName, "-", "", -1)
	masterData["api"] = "/" + camelTableName
	masterData["definition"] = definitions
	masterData["app"] = appName
	return masterData
}

func CreateDataService(appName, tableName string, jsonData JSONFile, authToken string) string {
	var creator CreateDS
	var d CreateResponse
	creator.App = appName
	creator.Description = nil
	creator.Name = tableName
	JSONpayload, err := json.Marshal(creator)
	CreateRequest, _ := http.NewRequest("POST", jsonData.OdpDetails.URI+"/api/a/sm/service/", bytes.NewBuffer(JSONpayload))
	CreateRequest.Header.Set("Content-Type", "application/json")
	CreateRequest.Header.Set("authorization", authToken)
	CreateResponse, err := httpRetry(CreateRequest, 10, 3, 2, HTTP5xxErrorsAndUnauthorize)
	if err != nil {
		log.Println(err)
	}

	defer CreateResponse.Body.Close()
	log.Println("DS Creation Returned: ", CreateResponse.StatusCode)
	body, err := ioutil.ReadAll(CreateResponse.Body)
	if err != nil {
		log.Println(err)
	}
	err = json.Unmarshal([]byte(string(body)), &d)
	if err != nil {
		log.Println(err)
	}
	return d.ID
}

func FillDataInService(URI string, jsonString []byte, authToken string) {
	AddDataRequest, _ := http.NewRequest("PUT", URI, bytes.NewBuffer(jsonString))
	AddDataRequest.Header.Set("Content-Type", "application/json")
	AddDataRequest.Header.Set("authorization", authToken)
	AddDataResponse, err := httpRetry(AddDataRequest, 10, 3, 2, HTTP5xxErrorsAndUnauthorize)
	if err != nil {
		log.Println(err)
	}
	defer AddDataResponse.Body.Close()
	log.Println("DS Data Fulfilment Returned: ", AddDataResponse.StatusCode)
}

func LoginAndReturnToken(odpStuff OdpDetails) (string, int, error) {

	key, _ := hex.DecodeString(EncKey)
	nonce, _ := hex.DecodeString(EncNonce)
	userName := string(decrypt(key, nonce, []byte(odpStuff.UserName)))
	password := string(decrypt(key, nonce, []byte(odpStuff.PassWord)))
	var payload LoginMessage
	payload.Username = userName
	payload.Password = password

	JSONpayload, err := json.Marshal(payload)

	if err != nil {
		log.Println("error:", err)
	}

	req, _ := http.NewRequest("POST", odpStuff.URI+"/api/a/rbac/login", bytes.NewBuffer(JSONpayload))
	req.Header.Set("Content-Type", "application/json")
	response, err := httpRetry(req, 10, 3, 2, HTTP5xxErrorsAndUnauthorize)
	if err != nil {
		return "", 400, err
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	if response.StatusCode != 200 {
		log.Println(string(body))
	}

	if response.StatusCode == 200 {
		var odpLoginResp ODPLoginResponse
		_ = json.Unmarshal(body, &odpLoginResp)
		token := "JWT " + odpLoginResp.Token
		return token, 200, nil
	}
	return "", response.StatusCode, nil
}

func decrypt(key []byte, nonce []byte, encryptedtext []byte) []byte {
	ciphertext, _ := hex.DecodeString(string(encryptedtext))
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	plaintext, err := aesgcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		panic(err.Error())
	}
	return plaintext
}
