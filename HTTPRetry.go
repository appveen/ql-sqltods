package main

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

// Deifining HTTP Retry Strategyz
const (
	HTTP5xxErrors               = "HTTP5xxErrors"               // all 5xx error codes
	HTTP5xxErrorsAndUnauthorize = "HTTP5xxErrorsAndUnauthorize" // 5xx and 403 error
)

// Retry Function which will take request , timeout , retry attempt and retry Delay as Input
func httpRetry(req *http.Request, timeOut time.Duration, retryAttempts int, retryDelay time.Duration, retryStrategy string) (*http.Response, error) {

	// Retry attempts variable
	attempts := 0

	// Handling of Input Body which will be used for retry
	var data []byte
	var err error

	if req.Body != nil {
		data, err = ioutil.ReadAll(req.Body)
		if err != nil {
			data = []byte("")
		}

	}
	// close the original body, we don't need it anymore
	if req.Body != nil {
		if err := req.Body.Close(); err != nil {
			return nil, err
		}
	}

	//Http Transaport with self signed certificates and Http Timeout

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	var netClient = &http.Client{
		Timeout:   time.Second * timeOut,
		Transport: tr,
	}

	// Retry Login group

	for {

		var httpErrorMessage string
		req.Body = ioutil.NopCloser(bytes.NewReader(data)) // reset the body
		attempts++
		statusCode := 0
		httpSuccess := false

		// Make HTTP Call
		response, err := netClient.Do(req)

		// close the original body, we don't need it anymore
		if req.Body != nil {
			if err := req.Body.Close(); err != nil {
				return nil, err
			}
		}

		//log.Println(response, "   ", err)

		// in case of success reply back

		if response != nil {
			//fmt.Println(response.StatusCode)
			// defer response.Body.Close()
			statusCode = response.StatusCode
		}

		if err == nil && statusCode == http.StatusOK {
			httpSuccess = true
			//fmt.Println("httpSuccess", httpSuccess)

			tr.CloseIdleConnections()
			return response, nil
		}

		if err != nil {
			fmt.Println(err)
			httpErrorMessage = err.Error()

		}

		// Making decision whether to retry or not
		if !httpSuccess {
			retry := retryDecision(statusCode, httpErrorMessage, attempts, retryAttempts, retryStrategy)
			if !retry {
				tr.CloseIdleConnections()
				return response, err
			}
		}

		time.Sleep(retryDelay * time.Second)
		fmt.Println("After Delay")
	}
}

func retryDecision(responseStatusCode int, httpErrorMessage string, attempts int, retryAttempts int, retryStrategy string) bool {

	retry := false
	fmt.Println("Retry Decision  - ", responseStatusCode, attempts, retryAttempts, retryStrategy)
	var errorCodeForRetry []int

	if retryStrategy == HTTP5xxErrors {
		errorCodeForRetry = []int{http.StatusInternalServerError, http.StatusNotImplemented, http.StatusBadGateway, http.StatusServiceUnavailable, http.StatusGatewayTimeout}
	}

	if retryStrategy == HTTP5xxErrorsAndUnauthorize {
		errorCodeForRetry = []int{http.StatusInternalServerError, http.StatusUnauthorized, http.StatusNotImplemented, http.StatusBadGateway, http.StatusServiceUnavailable, http.StatusGatewayTimeout}
	}

	for _, code := range errorCodeForRetry {
		if code == responseStatusCode && attempts <= retryAttempts {
			retry = true
		}
	}

	if strings.Contains(httpErrorMessage, "target machine actively refused it") && attempts <= retryAttempts {
		retry = true
	}

	fmt.Println("Retry Decision  - ", retry)

	return retry

}

// func testmain() {

// 	fmt.Println("Test")

// 	 var netClient = &http.Client{
// 	 	Timeout: time.Second * 3,
// 	 }

// 	fmt.Println(netClient)

// 	req, _ := http.NewRequest("GET", "http://:8790", nil)
// 	response, err := netClient.Do(req)
// 	response, err := httpRetry(req, 5, 0, 1)
// 	 response, err := netClient.Get("http://:8790")

// 	fmt.Println("Response - ", response)
// 	fmt.Printf("%+v", response)
// 	fmt.Println("Error  - ", err)

// }
