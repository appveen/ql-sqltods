package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"os"

	_ "github.com/denisenkom/go-mssqldb"
	_ "github.com/go-sql-driver/mysql"
	// _ "gopkg.in/goracle.v2"
)

var EncKey = os.Getenv("ENC_KEY")
var EncNonce = os.Getenv("ENC_NONCE")

type AppConfigProperties map[string]string

func main() {
	props, err := ReadPropertiesFile("config.properties")
	if err != nil {
		log.Println("Error while reading properties file")
	}
	jsonData := ReadJSONFile("config.json")
	var db *sql.DB
	authToken, _, err := LoginAndReturnToken(jsonData.OdpDetails)
	if err != nil {
		log.Println(err)
	}
	db = ConnectToMySQL(db, jsonData)
	defer db.Close()
	var rows *sql.Rows
	for _, val := range jsonData.OdpDetails.AppDetails {
		definitions := make(map[string]interface{})
		insideID := make(map[string]interface{})
		IDproperties := make(map[string]string)
		tableName := val.Table
		appName := val.App
		rows = GetColumnDetails(rows, db, tableName, jsonData.SQLDetails.Type)
		definitions = AppendID(insideID, IDproperties, definitions, tableName)
		definitions = AppendRest(rows, props, definitions, tableName, jsonData.SQLDetails.Type)
		masterData := CreateMaster(definitions, tableName, appName)
		jsonString, err := json.Marshal(masterData)
		if err != nil {
			log.Println(err)
		}
		log.Println(string(jsonString))
		serviceID := CreateDataService(appName, tableName, jsonData, authToken)
		FillDataInService(jsonData.OdpDetails.URI+"/api/a/sm/service/"+serviceID, jsonString, authToken)

		// rows = nil
		// rows.Close()
	}
}
