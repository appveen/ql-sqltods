package main

type CreateDS struct {
	Name        string      `json:"name"`
	Description interface{} `json:"description"`
	App         string      `json:"app"`
}

type CreateResponse struct {
	ID string `json:"_id"`
}

type JSONFile struct {
	OdpDetails `json:"odpInfo"`
	SQLDetails `json:"sqlInfo"`
}
type OdpDetails struct {
	URI        string       `json:"url"`
	UserName   string       `json:"userName"`
	PassWord   string       `json:"passWord"`
	AppDetails []AppDetails `json:"appDetails"`
}
type SQLDetails struct {
	Type         string `json:"type"`
	Username     string `json:"username"`
	Password     string `json:"password"`
	InstanceName string `json:"instanceName"`
	SQLURI       string `json:"url"`
	Port         int    `json:"port"`
	Database     string `json:"msDatabase"`
}
type AppDetails struct {
	Table string `json:"table"`
	App   string `json:"odpApp"`
}
type LoginMessage struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type ODPLoginResponse struct {
	Token string `json:"token"`
}
